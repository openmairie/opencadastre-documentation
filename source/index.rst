.. opencadastre documentation master file.


================================
OpenCadastre 2.1 - Documentation
================================

L’application OpenCadastre se propose :

* d’intégrer les données majic3 et le plan cadastral informatisé fournis par le Ministère de l’Économie et des Finances dans une base de données SQL  en

  - correspondant au plus près à la structure de données sources
  - conservant les noms de colonnes sources intégrant des contraintes relationnelles
  
* de proposer une représentation cartographique interactive en s’appuyant sur le standard WMS
* de permettre l’intégration de ses données dans le système d’informations 
* de fournir des géométries élémentaires permettant d’alimenter des paniers 


Bonne lecture et n'hésitez pas à nous faire part de vos remarques à l'adresse
suivante : contact@openmairie.org !

   
.. toctree::
   :numbered:
   :maxdepth: 3

   ergonomie/index.rst
   utilisation/index.rst
   traitement/index.rst
   installation/index.rst


Contributeurs
=============

* `Alain Baldachino <alain.baldachino@ville-vitrolles13.fr>`_

.. note::

   Cette création est mise à disposition selon le Contrat Paternité-Partage des
   Conditions Initiales à l'Identique 2.0 France disponible en ligne
   http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
   Creative Commons, 171 Second Street, Suite 300, San Francisco,
   California 94105, USA.
