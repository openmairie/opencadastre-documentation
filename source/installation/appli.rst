.. _appli:

###############################
Initialisation de l'application
###############################


Modifier les paramètres de connexion à la base ainsi que le nom du schéma dans le fichier dyn/ database.inc.php

.. image:: ../_static/installation/init_appli_database_inc.jpg

Le fichier dyn/var.inc.php doit également être modifié comme suit

.. image:: ../_static/installation/init_appli_var_inc.jpg

Les traitements d’intégration des données s’appuient sur les utilitaires shp2pgsql  et GDAL (version 1.9 mini).
L’utilitaire shp2pgsql est installé par défaut sur le serveur Postgres avec l’extension postgis.
La bibliothèque GDAL est généralement présente sur les serveurs cartographiques. C’est le cas pour la configuration préconisée dans le cadre d’OpenMairie avec l’utilisation de QGIS Server.
