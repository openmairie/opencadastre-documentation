.. _qgis:

################################################
Initialisation du fichier de représentation qgis
################################################


La représentation cartographique ainsi que le flux wms associé d'openCadatre s'appuient sur Qgis.

Nous allons voir dans cette rubrique comment configurer le fichier qgis d'openCadastre.


- Copier le fichier app/qgis/modele.qgs en le renommant dans votre répertoire cible (tel qu'indiqué dans la variable wms_opencadastre_url du fichier d'initialisation de la base dyn/pgsql/install.sql),
  dans notre exemple c:\wamp\qgis44\opencadastre\projet.qgs

- Copier le répertoire app/qgis/svg dans le même répertoire cible.

- Puis procéder au remplacement des chaines de caractères dans le fichier .qgs comme indiqué dans app/qgis/README.txt

.. image:: ../_static/installation/init_qgis_readme.jpg

.. image:: ../_static/installation/init_qgis_replace.jpg



