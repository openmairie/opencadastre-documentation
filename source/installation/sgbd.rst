.. _sgbd:

####################################
Initialisation de la base de données
####################################


Créer une base postgres avec l’extension Postgis (2.xx) (ex : openmairie) encodée en UTF-8.

Modifier le script data/pgsql/install.sql

.. image:: ../_static/installation/init_sgbd_install_sql.jpg

L’application s’intalle par défaut dans le schéma opencadastre créé par ce script.

La variable wms_opencadastre_url contient l’url du flux WMS fourni par OpenCadastre.

Dans cet exemple le serveur wms utilisé en QGIS Server dont l’url est http://localhost/cartes/qgis/qgis_mapserv.fcgi.exe?SERVICE=WMS&VERSION=1.3.0

Le fichier qgis du cadastre est C:/wamp/qgis44/opencadastre/projet.qgs (chemin absolu fonction de serveur hébergant QGIS Server).

Si vous ètes sous Windows vous pouvez lancer le script d’initialisation avec data/pgsql/install.bat

.. image:: ../_static/installation/init_sgbd_install_bat.jpg

Dans notre cas l’application sera installée dans la base openmairie sur le serveur localhost.

Le script s’execute en moins d’une minute.

.. image:: ../_static/installation/init_sgbd_install_exe.jpg

Nous obtenons avec PGAdmin le résultat suivant

.. image:: ../_static/installation/init_sgbd_install_res.jpg
