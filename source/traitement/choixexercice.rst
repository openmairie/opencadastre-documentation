.. _choixexercice:

###########################
Choix de l'exercice courant
###########################

Ce traitement permet de 

- définir l'exercice courant des données

- remplir les tables d'exportations conséquence

 - :ref:`dgi_adresse_parcelle`
 - :ref:`dgi_adresse_uf`
 - :ref:`dgi_batiment`
 - :ref:`dgi_parcelle`
 - :ref:`dgi_uf`
  
Ouvrir la page du traitement via le menu Traitements / Choix exercice courant

.. image:: ../_static/traitement/trait_exercice_menu.jpg

Choisir l'exercice correspondant au millésime des données importées et cliquer sur "lancer le traitement" (1 min).

.. image:: ../_static/traitement/trait_exercice_exec.jpg
