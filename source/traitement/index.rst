.. _index:

###########
Traitements
###########


Nous vous proposons dans ce chapitre de mettre en oeuvre les traitements d'intégration et de formatage des données.

Le processus annuel d'intégration doit respecter l'ordre suivant.

Dans un premier temps nous procédons à l'intégration et au formatage des données brutes fournies par la DGI

- importation majic 3
- importation Edigéo (plan cadastral numérisé)

Nous produisons ensuite les données dérivées à l'aide des traitements suivants

- génération UF (Unité Foncière)
- choix de l'exercice couranrt
    
	
Enfin nous avons la possibilité 
- de purger la base de données des données obsolètes	(suppression exercice)
- d'anonymiser tout ou partie des données propriétaires 

.. toctree::

 importmajic3.rst
 importedigeo.rst
 generationuf.rst
 choixexercice.rst
 suppressionexercice.rst
 anonymisation.rst
