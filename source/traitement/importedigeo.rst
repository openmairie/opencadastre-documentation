.. _importedigeo:

#######################################################
Importation du plan cadastral numérisé au format EDIGEO
#######################################################

Ce traitement permet d'importer et de formater les données du plan cadastral numérisé fournies au format EDIGEO.

Il doit être lancé après le traitement d'importation des données majic 3.

Le serveur doit disposer des outils ogr2ogr, fourni avec GDAL,  et shp2pgsql, fourni avec l'extension postgis de postgresql.

La livraison du plan cadastral numérisé au format EDIGEO se compose de plusieurs lot de données représentés par des fichiers avec les extensions .THF, .VEC, .GEN, .GEO, .QAL, .DIC, .SCD.

La librairie GDAL, à l'aide de l'utilitaire ogr2ogr, nous permet de convertir ces données au format shape, format que nous pouvons importer dans postgres avec l'utilitaire shp2pgsql.

- Copier les fichiers dans le répertoire serveur que vous avez spécifié dans la variable $import_edigeo_chemin du fichier dyn/var.inc.php.

Ouvrir la page du traitement via le menu Traitements / Import. Edigeo

.. image:: ../_static/traitement/trait_edigeo_menu.jpg

Modifier si nécessaire les champs:

- version des fichiers: il s'agit du millésime du format des données majic 3
- concerne l'exercice: il s'agit du millésime des données majic 3
- Département Direction (ccodep||ccodir majic3): 3 premiers caractères du fichier majic3 NBAT
- chemin des fichiers edigeo: dossier serveur contenant les fichiers edigeo, vu du serveur
- Systèmes de coordonnées de références :
  - source : en général il s'agit du RGF93 / Lambert 93 noté EPSG:2154 pour la métropole
  - destination : EPSG:2154
- chemin complet ogr2ogr: nom complet de l'utilitaire ogr2ogr, vu du serveur
- chemin complet shp2pgsql: nom complet de l'utilitaire shp2pgsql, vu du serveur
- chemin des fichiers temporaires: répertoire de travail vu du serveur; le traitement a besoin de créer/modifier/supprimer des fichiers et des répertoire, le serveur apache doit donc disposer de tous les droits sur ce dernier
- numéro de lot: 3 caractères qui seront repris dans la base de données afin de pouvoir gérer plusieurs lots de données.

Pour compléter les deux premiers champs, prenons l'exemple suivant: fin 2013 nous est transmis les données arrêtées au 31/12/2012 au format 2013 soit, dans le premier champ 2013 et dans le second 2012.

Le même numéro de lot devra être repris du traitement d'importation des fichiers majic 3 d'assurer l'intégrité des données.

Cette procédure s’appuie sur les canevas de script sql présent dans les répertoires data/pgsql/COMMUN et data/pgssql/[VERSION], version correspondant à votre saisie dans le champ « Version des fichiers »

Il y a 6 étapes obligatoires que vous pouvez exécuter l’une après l’autre ou toutes automatiquement (Enchainer les traitements). Cette dernière possibilité n’est toutefois pas préconisée.

- Choisir la première étape et cliquer sur "lancer le traitement" 

1 - Convertir en shapes: convertis chaque lot de données du format edigeo au format shape à l'aide de l'outil ogr2ogr, vous pouvez voir le résultat dans le répertoire SHP1 du répertoire de travail.
  (6 min pour 80 lots)

.. image:: ../_static/traitement/trait_edigeo_etape1.jpg

- Cliquer sur "2"

2 - Fusionner les shapes: fusionne les différents lots de données en un seul, vous pouvez voir le résultat dans le répertoire SHP2 du répertoire de travail.
  (9 min pour 80 lots)

.. image:: ../_static/traitement/trait_edigeo_etape2.jpg

- Cliquer sur "3"

3 - Supprimer les tables importées et les données: supprime les données préalablement importées (critères "Concerne l'exercice" et "numéro de lot")
  (< 1mn si vide)

.. image:: ../_static/traitement/trait_edigeo_etape3.jpg

- Cliquer sur "4"

4 - Importer les shapes: importe les fichiers shape dans des tables temporaires après convertion en sq, vous pouvez voir les scripts dans le répertoire SQL du répertoire de travail.
  (3mn pour 80 lots)

.. image:: ../_static/traitement/trait_edigeo_etape4.jpg

- Cliquer sur "5"

5 - Formatage des données: insérer et formate les données brutes importées à l'étape 4 en respectant le modèle de données openCadastre

.. image:: ../_static/traitement/trait_edigeo_etape5.jpg

- Cliquer sur "6"

6 - Supprimer toutes les données temporaires : supprime les données brutes importées à l'étape 4 et supprime les répertoires de travail préalablement créés
  (environ 2mn)

.. image:: ../_static/traitement/trait_edigeo_etape6.jpg
