.. _suppressionexercice:

########################################################
Suppression des données importés relatives à un exercice
########################################################

Ce traitement permet de supprimer à la fois les données majic3 mais aussi les données du plan cadastral pour un exercice donné.

Ouvrir la page du traitement via le menu Traitements / Import. Majic3

.. image:: ../_static/traitement/trait_sup_exercice_menu.jpg

Modifier si nécessaire les champs:

- version des fichiers: il s'agit du millésime du format des données majic 3
- concerne l'exercice: il s'agit du millésime des données majic 3
- numéro de lot: 3 caractères qui seront repris dans la base de données afin de pouvoir gérer plusieurs lots de données

Pour compléter les deux premiers champs, prenons l'exemple suivant: fin 2013 nous est transmis les données arrêtées au 31/12/2012 au format 2013 soit, dans le premier champ 2013 et dans le second 2012.

Il y a 4 étapes obligatoires que vous pouvez exécuter l’une après l’autre ou toutes automatiquement (Enchainer les traitements). Cette dernière possibilité n’est toutefois pas préconisée.

- Choisir la première étape et cliquer sur "lancer le traitement" 

1 - Supprimer les données Edigeo : suppression du plan cadastral numérisé (tables geo_*) . 
  (environ 6mn)

.. image:: ../_static/traitement/trait_sup_exercice_etape1.jpg

- Cliquer sur "2"

2 - Suppression des contraintes relationnelles : ôte les contraintes référentielles
  (environ 1mn)

.. image:: ../_static/traitement/trait_sup_exercice_etape2.jpg

- Cliquer sur "3"

3 - Purges des données majic3 : supprime les données majic 3
  (environ 1mn)

.. image:: ../_static/traitement/trait_sup_exercice_etape3.jpg

- Cliquer sur "4"

4 - Rétablissement des contraintes relationnelles : repose les contraintes réferentielles ôtées à l'étape 2 afin d'assurer l'intégrité des données
  (environ 1mn)

.. image:: ../_static/traitement/trait_sup_exercice_etape4.jpg
