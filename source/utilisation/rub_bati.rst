.. _rub_bati:

#################
Propriétés baties
#################

Important: les notes sont extraites de la documentation fournie avec les données.

.. note ::
  
  Le fichier bâti regroupe par direction l'ensemble des informations concernant le local et la partie
  d’évaluation (PEV). La PEV est l’élément de gestion en bâti. Une PEV correspond à une fraction de local
  caractérisée par son affectation et faisant l’objet d’une évaluation distincte. Un local est constitué d’au
  moins une PEV.
  
  Le local est identifié par son numéro invariant, par son indicatif cadastral complété des numéros de
  bâtiment, d'escalier, de niveau et de porte (ba, es, ni, ordre) ou par son adresse complétée de ba, es, ni,
  ordre.
  
  Il permet de disposer pour un local donné de son descriptif, de son évaluation et des bases de taxation.
  L'attribution du local à son propriétaire est assurée par l'intermédiaire du compte communal.

------------------
Modèle relationnel
------------------

.. image:: ../_static/modeles/bati.jpg


------------------
Les tables
------------------

.. _local00:

******************************
Identifiant du local (local00)
******************************

.. note ::

  c’est l’article tête du local. Cet article a un indicatif tronqué au numéro invariant; il
  comprend l'indicatif cadastral (section, plan, ba, es, ni, ordre) et l'adresse du local (voie, voirie). Sa
  présence est obligatoire.

.. image:: ../_static/utilisation/local00.jpg

Champ clé primaire :

- local00 character varying(14) NOT NULL: code identifiant du local ([annee][invar] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - invar character varying(10): numéro invariant

* Identifiant cadastral

  - ccopre character varying(3): préfixe de section ou quartier servi pour les communes associées
  - ccosec character varying(2): lettres de section
  - dnupla character varying(4): numéro de plan
  - dnubat character varying(2): lettre de bâtiment
  - descr character varying(2): numéro d’entrée 
  - dniv character varying(2): niveau étage 
  - dpor character varying(5): numéro de local
  
* Identifiant adresse

  - ccoriv character varying(4): Code Rivoli de la voie
  - ccovoi character varying(5): Code Majic2 de la voie
  
* Voirie

  - dnvoiri character varying(4): Numéro de voirie
  - dindic character varying(1): indice de répétition
  - ccocif character varying(4): code du cdi/cdif (code topad)
  - dvoilib character varying(30): libelle de la voie
  - cleinvar character varying(1): clé alpha no invariant
  - *locinc character varying(1): code local sans évaluation (indisponible)*

* Autre

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`parcelle`: parcelle character varying(19) - parcelle à laquelle est rattachée le local
- :ref:`voie`: voie character varying(15) - voie à laquelle est adossée le local
 
Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- local10: :ref:`local10`
- lotslocaux: :ref:`lotslocaux`

.. _local10:

*****************************
Descriptif du local (local10)
*****************************

.. note ::

  de même indicatif que l'article local00, il contient les données générales du local. Sa
  présence, suite à l'article local00, est obligatoire

.. image:: ../_static/utilisation/local10.jpg

Champ clé primaire :

- local10 character varying(14) NOT NULL: code identifiant du descriptif du local ([annee][invar] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - invar character varying(10): numéro invariant
  
* Identifiant cadastral

  - ccopre character varying(3): préfixe de section ou quartier servi pour les communes associées
  - ccosec character varying(2): lettres de section
  - dnupla character varying(4): numéro de plan
  - Identifiant adresse
  - ccoriv character varying(4): Code Rivoli de la voie
  - ccovoi character varying(5): Code Majic2 de la voie
  - dnvoiri character varying(4): Numéro de voirie

* Divers

  - gpdl character varying(1): indicateur d’appartenance à un lot de pdl - 1 = oui, sinon 0
  - dsrpar character varying(1): lettre de série rôle
  - dnupro character varying(6): compte communal de propriétaire
  - jdatat date: date d’acte de mutation - jjmmaaaa
  - dnufnl character varying(6): compte communal de fonctionnaire logé - redevable de la tom
  - *ccitlv character varying(1): local imposable à la taxe sur les locaux vacants (indisponible)*
  - gtauom character varying(2): zone de ramassage des ordures ménagères - P RA RB RC RD ou blanc
  - dcomrd character varying(3): Pourcentage de réduction sur tom
  - dvltrt integer: Valeur locative totale retenue pour le local
  - ccoape character varying(4): Code NAF pour les locaux professionnels
  - cc48lc character varying(2): Catégorie de loi de 48
  - dloy48a integer: Loyer de 48 en valeur de l’année
  
* Indisponible

  - *dnupas character varying(8): no passerelle TH/TP (indisponible)*
  - *gnexcf character varying(2): code nature exo ecf (indisponible)*
  - *dtaucf character varying(3): taux exo ecf (indisponible)*
  
* Divers

  - cchpr character varying(1): Top indiquant une mutation propriétaire - * ou blanc
  - jannat character varying(4): Année de construction
  - dnbniv character varying(2): Nombre de niveaux de la construction
  - postel character varying(1): Local de Poste ou France Telecom - X, Y, Z, ou blanc

* Indisponible

  - jdatcgl date: Date changement évaluation
  - *dnutbx character varying(6): no gestionnaire déclarant taxe bureaux (indisponible)*
  - *dvltla character varying(9): VL totale du local actualisée (indisponible)*
  - *janloc character varying(4): Année de création du local (indisponible)*
  - *ccsloc character varying(2): Code cause création du local (indisponible)*
  - *fburx integer: Indicateur présence bureaux (indisponible)*

* Divers

  - gimtom character varying(1): Indicateur imposition OM exploitable à partir de 2002  - D, E, V ou blanc
  - cbtabt character varying(2): Code exonération HLM zone sensible - ZS, ZT ou blanc
  - jdtabt character varying(4): Année début d’exonération ZS
  - jrtabt character varying(4): Année fin d’exonération ZS
  - *jacloc character varying(4): Année d’achèvement du local (indisponible)*
  - cconac character varying(5): Code NACE pour les locaux professionnels

* Autre

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`local00`: local00 character varying(14) -Identifiant du local
- :ref:`parcelle`: parcelle character varying(19) - parcelle à laquelle est rattachée le local
- :ref:`voie`: voie character varying(15) - voie à laquelle est rattachée le local
- :ref:`comptecommunal`: comptecommunal character varying(12) - comptecommunal auquel est rattaché le local
- ccoeva_ character varying(1): code évaluation - A B C D E T
- dteloc_ character varying(1): type de local - 1 à 8
- ccoplc_ character varying(1): Code de construction particulière - R U V W X Y Z ou blanc
- cconlc_ character varying(2): Code nature de local
- top48a_ character varying(1): top taxation indiquant si la pev est impose au loyer ou a la vl - 1 = loyer o = vl
- dnatlc_ character varying(1): Nature d occupation - A P V L T D tableau 2.3.6
- hlmsem_ character varying(1): Local appartenant à hlm ou sem - 5 = hlm, 6 = sem, sinon blanc
- dnatcg_ character varying(2): Code nature du changement évaluation

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- lotslocaux: :ref:`lotslocaux`
- pev: :ref:`pev`

.. _pev:

***********************
Descriptif de PEV (pev)
***********************

.. note ::

  Un local peut être constitué de une à N parties PEV. Chaque PEV peut être décrite par plusieurs articles
  
  il contient les éléments d’évaluation d’une PEV, ainsi que certaines informations de descriptif comme 
  les coefficients d’entretien et de situation générale qui ont un impact sur l’évaluation. Sa présence, 
  suite à l'article local10, est obligatoire.

.. image:: ../_static/utilisation/pev.jpg

Champ clé primaire :

- pev character varying(17) NOT NULL: code identifiant du descriptif la pev ([annee][invar][dnupev] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département
- ccodir character varying(1): Code direction
- ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
- invar character varying(10): numéro invariant
- dnupev character varying(3): Numéro de pev
- ccostb character varying(1): lettre de série tarif bâtie ou secteur locatif - A à Z
- dcapec character varying(2): Catégorie
- dcetlc numeric(3,2): Coefficient d entretien
- *dcsplc character varying(3): Coefficient de situation particulière (indisponible)*
- dsupot integer: Surface pondérée - Présence non systématique
- dvlper integer: Valeur locative de la pev, en valeur de référence (1970) sauf pour les établissements de code évaluation A
- dvlpera integer: Valeur locative de la pev, en valeur de l’année
- *libocc character varying(30): nom de l occupant (indisponible)*
- ccthp character varying(1): Code occupation à la Th ou à la TP
- retimp character varying(1): Top : retour partiel ou total à imposition
- dnuref character varying(3): Numéro de local type
- *rclsst character varying(32): Données reclassement (indisponible)*
- gnidom character varying(1): Top : pev non imposable (Dom)
- dcsglc character varying(3): Coefficient de situation générale
- *ccogrb character varying(1): Code groupe bâti révisé (indisponible)*
- *cocdi character varying(4): Code cdi topad (indisponible)*
- *cosatp character varying(3): Code service topad (indisponible)*
- *gsatp character varying(1): Nature service gérant tp (indisponible)*
-  *clocv character varying(1): Indicateur local vacant (indisponible)*
- *dvltpe integer: VL totale de la pev majic*
- dcralc character varying(3): correctif d’ascenseur
- topcn integer: Top construction nouvelle
- tpevtieom integer: Top Local passible de la TEOM
- dcsplca character varying(5): Coefficient de situation particulière
- dcsglca character varying(5): Coefficient de situation générale
- dcralca character varying(5): Correctif d'ascenseur
- lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`local10`: local10 character varying(14) - local de rattachement de la pev
- ccoaff_ character varying(1): Affectation de la pev - H P L S K
- gnexpl_ character varying(2): Nature d’exonération permanente

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- pevdependances: :ref:`pevdependances`
- pevexoneration: :ref:`pevexoneration`
- pevprincipale: :ref:`pevprincipale`
- pevprofessionnelle: :ref:`pevprofessionnelle`
- pevtaxation: :ref:`pevtaxation`

.. _pevtaxation:

*****************************
Taxation de PEV (pevtaxation)
*****************************

.. note ::

  unique et obligatoire pour chaque PEV, c'est l'article taxation de la PEV. 
  
  De même indicatif que l'article pev, il contient par collectivité concernée 
  la part de valeur locative imposée en valeur 70 et en valeur de l’année, ainsi 
  que la base d'imposition de l’année.
  
  Pour les établissements industriels, l’article est également unique; l’année 
  d’immobilisation présente devant le numéro de PEV est servie en « High Value ».

.. image:: ../_static/utilisation/pevtaxation.jpg

Champ clé primaire :

- pevtaxation character varying(17) NOT NULL: code identifiant de la taxation de pev ([annee][invar][dnupev] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - invar character varying(10): numéro invariant
  - janbil character varying(4): Année d’immobilisation - High value pour ets. Industriels
  - dnupev character varying(3): Numéro de pev
  
* Données calculées de taxation

  - co_vlbai integer: Commune - Part de VL imposée (valeur70)
  - co_vlbaia integer: Commune - Part de VL imposée (valeur de l’année)
  - co_bipevla integer: Commune - Base d’imposition de la pev(valeur de l’année)
  - de_vlbai integer: Département - Part de VL imposée (valeur70)
  - de_vlbaia integer: Département - Part de VL imposée (valeur de l’année)
  - de_bipevla integer: Département - Base d’imposition de la pev(valeur de l’année)
  - re_vlbai integer: Région (avant 2012) - Part de VL imposée (valeur70)
  - re_vlbaia integer: Région (avant 2012) - Part de VL imposée (valeur de l’année)
  - re_bipevla integer: Région (avant 2012) - Base d’imposition de la pev(valeur de l’année)
  - tse_vlbai integer: TSE (à partir de 2012) - Part de VL imposée (valeur70)
  - tse_vlbaia integer: TSE (à partir de 2012) - Part de VL imposée (valeur de l’année)
  - tse_bipevla integer: TSE (à partir de 2012) - Base d’imposition de la pev(valeur de l’année)
  - gp_vlbai integer: Groupement de commune - Part de VL imposée (valeur70)
  - gp_vlbaia integer: Groupement de commune - Part de VL imposée (valeur de l’année)
  - gp_bipevla integer: Groupement de commune - Base d’imposition de la pev(valeur de l’année)
  - bateom integer: base ordures ménagères
  - baomec integer: base ordures ménagères écrétée
  - mvltieomx integer: Montant TIEOM
  - pvltieom real: Ratio VL n-1 de la PEV / VL n-1 collectivité
  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`pev`: pev character varying(17) - Descriptif de PEV (pev)

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant
 
.. _pevexoneration:

***********************************
Exonération de PEV (pevexoneration)
***********************************

.. note ::

  facultatif, il contient l'exonération temporaire de la PEV à laquelle il est rattaché.
  
  Une PEV peut théoriquement bénéficier de 0 à 15 exonérations différentes. L'indicatif est
  entièrement servi, le numéro d'ordre varie de 001 a 015.
  
  Spécificité des établissements industriels :
  
  - L’année d’immobilisation est présente juste avant les numéro de PEV. Il peut théoriquement y avoir
    de 0 à 15 exonérations différentes par année d’immobilisation.
  - Les exonération permanentes sont gérées comme des exonérations temporaires avec une année de
    retour à imposition à zéro.

.. image:: ../_static/utilisation/pevexoneration.jpg

Champ clé primaire :

- pevexoneration character varying(24) NOT NULL: code identifiant de l'exonération de pev ([annee][invar][dnupev][dnuexb][janbil] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - invar character varying(10): numéro invariant
  - janbil character varying(4): Année d’immobilisation - High value pour ets. Industriels
  - dnupev character varying(3): Numéro de pev
  - dnuexb character varying(3): Numéro d’ordre de l’article - 001 à 015
  
* Données des exonérations

  - pexb numeric(5,2): Taux d’exonération accordée
  - jandeb character varying(4): année de début d’exonération
  - janimp character varying(4): année de retour à imposition
  - *vecdif character varying(9): montant saisi de l’EC bénéficiant exo (indisponible)*
  - *vecdifa character varying(9): vecdif multiplié par coeff (indisponible)*
  - *fcexb character varying(9): Fraction EC exonérée (indisponible)*
  - *fcexba character varying(9): fcexb multiplié par coeff (indisponible)*
  - *rcexba character varying(9): revenu cadastral exonéré (indisponible)*
  - dvldif2 integer: Montant de VL exonérée (valeur 70)
  - dvldif2a integer: Montant de VL exonérée (valeur de l’année)
  - fcexb2 integer: Fraction de VL exonérée (valeur 70)
  - fcexba2 integer: Fraction de VL exonérée (valeur de l’année)
  - rcexba2 integer: Revenu cadastral exonéré (valeur de l’année)
  
* Autre 

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`pev`: pev character varying(17) - Descriptif de PEV (pev)
- ccolloc_ character varying(2): Code de collectivité locale accordant l’exonération
- gnextl_ character varying(2): Nature d’exonération temporaire (et permanente pour ets. Industriels)

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :
- néant
 
.. _pevprincipale:

*******************************************************
Descriptif partie principale habitation (pevprincipale)
*******************************************************

.. note ::

  Descriptif de la partie principale habitation (DHA). Il a même numéro de PEV que l'article pev dont il 
  dépend et son numéro de descriptif est égal à « H ». C'est aussi le descriptif de construction accessoire 
  rattachée à une partie principale; dans ce cas, le numéro de descriptif peut varier de « HA » à « HZ ».
  
.. image:: ../_static/utilisation/pevprincipale.jpg

Champ clé primaire :

- pevprincipale character varying(20) NOT NULL: code identifiant de la partie principale d'habitation ([annee][invar][dnupev][dnudes] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - invar character varying(10): numéro invariant
  - dnupev character varying(3): Numéro de pev
  - dnudes character varying(3): Numéro d’ordre de descriptif - bHb, bHA...
  
* Occurrences positionnelles d'éléments incorporés

  - dep1_dsueic integer: Dépendance 1 - Surface réelle de l’élément incorporé
  - dep1_dcimei numeric(2,1): Dépendance 1 - Coefficient de pondération
  - dep2_dsueic integer: Dépendance 2 - Surface réelle de l’élément incorporé
  - dep2_dcimei numeric(2,1): Dépendance 2 - Coefficient de pondération
  - dep3_dsueic integer: Dépendance 3 - Surface réelle de l’élément incorporé
  - dep3_dcimei numeric(2,1): Dépendance 3 - Coefficient de pondération
  - dep4_dsueic integer: Dépendance 4 - Surface réelle de l’élément incorporé
  
* Eléments de confort

  - dep4_dcimei numeric(2,1): Dépendance 4 - Coefficient de pondération
  - geaulc character varying(1): Présence d’eau - O = oui, N = non
  - gelelc character varying(1): Présence d’électricité - O = oui, N = non
  - gesclc character varying(1): Présence d’escalier de service (appartement) - O = oui, N = non, blanc
  - ggazlc character varying(1): Présence du gaz - O = oui, N = non
  - gasclc character varying(1): Présence d’ascenseur (appartement) - O = oui, N = non, blanc
  - gchclc character varying(1): Présence du chauffage central - O = oui, N = non
  - gvorlc character varying(1): Présence de vide-ordures (appartement)  - O = oui, N = non, blanc
  - gteglc character varying(1): Présence du tout à l’égout - O = oui, N = non
  - dnbbai character varying(2): Nombre de baignoires
  - dnbdou character varying(2): Nombre de douches
  - dnblav character varying(2): Nombre de lavabos
  - dnbwc character varying(2): Nombre de WC
  - deqdha integer: Equivalence superficielle des éléments de confort Répartition des pièces
  
* Répartition des pièces

  - dnbppr character varying(2): Nombre de pièces principales
  - dnbsam character varying(2): Nombre de salles à manger
  - dnbcha character varying(2): Nombre de chambres
  - dnbcu8 character varying(2): Nombre de cuisines de moins de 9 m2
  - dnbcu9 character varying(2): Nombre de cuisines d’au moins 9 m2
  - dnbsea character varying(2): Nombre de salles d’eau
  - dnbann character varying(2): Nombre de pièces annexes
  - dnbpdc character varying(2): Nombre de pièces
  - dsupdc integer: Superficie des pièces
  
* Caractéristiques générales

  - dmatgm character varying(2): Matériaux des gros murs - 0 indéterminé 1 pierre 2 meulière 3 béton 4 briques 5 aggloméré 6 bois 9 autres
  - dmatto character varying(2): Matériaux des toitures - 0 indéterminé 1 tuiles 2 ardoises 3 zinc aluminium 4 béton
  - jannat character varying(4): Année d’achèvement 
  - detent character varying(1): état d’entretien - 1 bon 2 assez bon 3 passable 4 médiocre 5 mauvais
  - dnbniv character varying(2): Nombre de niveaux
  
* Autre 

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`pev`: pev character varying(17) - Descriptif de PEV (pev)
- cconad_ : dep1_cconad character varying(2) - Dépendance 1 - Nature de dépendance
- cconad_ : dep2_cconad character varying(2) - Dépendance 2 - Nature de dépendance
- cconad_ : dep3_cconad character varying(2) - Dépendance 3 - Nature de dépendance
- cconad_ : dep4_cconad character varying(2) - Dépendance 4 - Nature de dépendance

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant
 
.. _pevprofessionnelle:

*********************************************
Descriptif professionnel (pevprofessionnelle)
*********************************************

.. note ::
	
  Descriptif de la PEV non affectée à l'habitation (DNH). Il a même numéro de PEV que l'article 
  pev dont il dépend et son numéro de descriptif est égal a « P01 ». 
  Il est unique pour une PEV. S’agissant du descriptif d’une PEV professionnelle, cet article 
  n’existe que pour une PEV non affectée à l’habitation et est unique.

.. image:: ../_static/utilisation/pevprofessionnelle.jpg

Champ clé primaire :

- pevprofessionnelle character varying(20) NOT NULL: code identifiant la partie professionnelle de pev ([annee][invar][dnupev][dnudes] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département
- ccodir character varying(1): Code direction
- ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
- invar character varying(10): numéro invariant
- dnupev character varying(3): Numéro de pev
- dnudes character varying(3): Numéro d’ordre de descriptif - P01
- *vsupot character varying(9): surface pondérée (indisponible)*
- *vsurz1 character varying(9): Surface réelle totale zone 1 (indisponible)*
- *vsurz2 character varying(9): Surface réelle totale zone 2 (indisponible)*
- *vsurz3 character varying(9): Surface réelle totale zone 3 (indisponible)*
- vsurzt integer: Surface réelle totale
- *vsurb1 character varying(9): surface réelle des bureaux 1 (indisponible)*
- *vsurb2 character varying(9): surface réelle des bureaux 2 (indisponible)*
- lot character(3): lot d'importation des données
  
Champs clés étrangères :

- :ref:`pev`: pev character varying(17) - Descriptif de PEV (pev)

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant
 
.. _pevdependances:

*****************************************
Descriptif de dépendance (pevdependances)
*****************************************

.. note ::

  Descriptif de dépendance. Celle-ci peut être rattachée à une partie principale 
  (elle aura même numéro de PEV), elle peut être évaluée distinctement, elle peut 
  être rattachée à une dépendance évaluée distinctement. Le numéro d'ordre de son 
  indicatif varie de « 003 » a « 999 ».
.. image:: ../_static/utilisation/pevdependances.jpg

Champ clé primaire :

- pevdependances character varying(20) NOT NULL: code identifiant la partie dépendance de la pev ([annee][invar][dnupev][dnudes] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - invar character varying(10): numéro invariant
  - dnupev character varying(3): Numéro de pev
  - dnudes character varying(3): Numéro d’ordre de descriptif - 001, 002
  
* Caractéristiques générales

  - dsudep integer: Surface réelle de la dépendance
  - asitet character varying(6): Localisation (bat, esc, niv)
  - dmatgm character varying(2): Matériaux des gros murs - 0 à 9
  - dmatto character varying(2): Matériaux des toitures - 0 à 4 cf
  - detent character varying(1): état d'entretien - 1 à 5
  - geaulc character varying(1): Présence d'eau - O = oui, N = non
  - gelelc character varying(1): Présence d’électricité - O = oui, N = non
  - gchclc character varying(1): Présence du chauffage central - O = oui, N = non
  - dnbbai character varying(2): Nombre de baignoires
  - dnbdou character varying(2): Nombre de douches
  - dnblav character varying(2): Nombre de lavabos
  - dnbwc character varying(2): Nombre de WC
  - deqtlc integer: Equivalence superficielle des
  - dcimlc numeric(2,1): Coefficient de pondération - 1,0 - 0,2 à 0,6
  - dcetde numeric(3,2): Coefficient d entretien
  - *dcspde character varying(3): Coefficient de situation particulière (indisponible)*
  - dcspdea character varying(6): Coefficient de situation particulière
  
* Autre 

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`pev`: pev character varying(17) - Descriptif de PEV (pev)
- cconad_ character varying(2): Nature de dépendance - cf tableau des codes

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant
 
------------------
Les nomenclatures
------------------

.. _ccoeva:

- ccoeva: Méthode d’évaluation

.. _dteloc:

- dteloc: Type de local

.. _ccoplc:

- ccoplc: Construction Particulière

.. _cconlc:

- cconlc: Nature de local

.. _cconad:

- cconad: Nature de dépendances

.. _dnatlc:

- dnatlc: Nature d'occupation local

.. _dnatcg:

- dnatcg: Nature du changement d’évaluation

.. _ccoaff:

- ccoaff: Affectation de PEV

.. _gnexpl:

- gnexpl: Exonération permanente

.. _ccolloc:

- ccolloc: Collectivité

.. _gnextl:

- gnextl: Exonération temporaire

.. _top48a:

- top48a: Top taxation indiquant si la PEV est impose au loyer ou a la VL

.. _hlmsem:

- hlmsem: Si le local appartient à un organisme HLM ou à une société d’économie mixte, l'indicateur reçoit le code groupe de personne morale, respectivement 5 ou 6 ; dans les autres cas, il est à blanc.


