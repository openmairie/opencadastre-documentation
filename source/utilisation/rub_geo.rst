.. _rub_geo:

#######################
Plan cadastral numérisé
#######################

Important: les notes sont extraites de la documentation fournie avec les données.

------------------
Modèle relationnel
------------------
.. image:: ../_static/modeles/geo.jpg

------------------
Les tables
------------------

.. _geo_commune:

*********************
Commune (geo_commune)
*********************

.. note ::

  Territoire communal contenant un nombre entier de sections
  
  Son emprise est constituée à partir de l'union des sections qui la composent au moment de
  l’échange. 
  
  Le contour de l’objet "COMMUNE" est calculé automatiquement à partir des sections reçues, 
  lors d’un échange, même si l’objet "COMMUNE" a été transmis dans l’échange. 
  
.. image:: ../_static/utilisation/geo_commune.jpg

Champ clé primaire :

- geo_commune character varying(7) NOT NULL ( [annee][idu] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- idu character varying(3): Code INSEE
- tex2 character varying(80): Nom commune
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`commune`: commune character varying(10)

Table(s) dépendantes(s) :

- :ref:`commune`
- :ref:`geo_section`
- :ref:`geo_tline_commune`
- :ref:`geo_tpoint_commune`
- :ref:`geo_tsurf_commune`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_commune_sig.jpg

.. _geo_section:

********************************
Section cadastrale (geo_section)
********************************

.. note ::

  Territoire communal contenant un nombre entier de sections
  
  Partie du plan cadastral correspondant à une portion du territoire communal et
  comportant, suivant le cas, une ou plusieurs subdivisions de section.
  
  Son emprise est constituée à partir de l'union des subdivisions de section 
  qui la composent au moment de l’échange.

  Cet objet est obligatoire dans l’échange.
  
.. image:: ../_static/utilisation/geo_section.jpg

Champ clé primaire :

- geo_section character varying(12) NOT NULL ([annee][idu] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- idu character varying(8): Identifiant
- tex character varying(2): Lettre(s) de section
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
	  
Champs clés étrangères :

- :ref:`geo_commune`: commune character varying(7)


Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_parcelle`
- :ref:`geo_subdsect`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_section_sig.jpg
 
.. _geo_subdsect:

*************************************
Subdivision de section (geo_subdsect)
*************************************

.. note ::

  Portion de section cadastrale disposant de caractéristiques propres au regard notamment de :
   - son échelle ;
   - sa qualité ;
   - son mode de confection.
   
   Une section a au moins une subdivision de section. Cet objet correspond à la feuille cadastrale.
  
.. image:: ../_static/utilisation/geo_subdsect.jpg

Champ clé primaire :

- geo_subdsect character varying(14) NOT NULL([annee][idu] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- idu character varying(10): Identifiant
- eor integer: Échelle d'origine du plan (que le dénominateur)
- dedi date: Date d'édition ou du confection du plan
- icl integer: Orientation d'origine (en grade)
- dis date: Date d'incorporation PCI
- dred date: Date de réédition
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
  
Champs clés étrangères :

- :ref:`geo_section`: geo_section character varying(12) NOT NULL
- geo_qupl_ character varying(2): Qualité du plan
- geo_copl_ character varying(2): Mode de confection
- geo_inp_ character varying(2): Mode d'incorporation au plan

Table(s) dépendantes(s) :

- :ref:`geo_parcelle`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_subdsect_sig.jpg

.. _geo_parcelle:

***********************
Parcelle (geo_parcelle)
***********************

.. note ::

  Portion du territoire communal d'un seul tenant située dans une subdivision de section et
  appartenant à un même propriétaire.
  
  Une limite de parcelle peut être confondue avec une limite de lieudit, de subdivision de 
  section, de section, de commune, de département ou d'Etat lorsque ces limites ont des 
  tronçons communs.
  
  Certaines parcelles, incluses dans la voirie et en attente d'une régularisation juridique, ne
  figurent pas au plan cadastral.
  
  Les fichiers de plan cadastral informatisé délivrés par la DGI comprennent ces parcelles avec un
  code particulier. 

.. image:: ../_static/utilisation/geo_parcelle.jpg

Champ clé primaire :

- geo_parcelle character varying(16) NOT NULL ([annee][idu] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- idu character varying(12): Identifiant
- supf numeric(10,3): Contenance MAJIC
- coar character varying(2): Code arpentage
- tex character varying(4): Numéro parcellaire
- tex2 character varying(80): tex2 - non documenté
- codm character varying(80): codm - non documenté
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
  
Champs clés étrangères :

- :ref:`geo_section`: character varying(12) NOT NULL
- :ref:`geo_subdsect`: geo_subdsect character varying(14)
- geo_indp_ character varying(2): Figuration de la parcelle au plan
- :ref:`parcelle`: parcelle character varying(19)
- :ref:`parcelle`: parcelle_uf character varying(19) - unité foncière

Table(s) dépendantes(s) :

- :ref:`geo_batiment_parcelle`
- :ref:`geo_borne_parcelle`
- :ref:`geo_croix_parcelle`
- :ref:`geo_label`
- :ref:`geo_numvoie_parcelle`
- :ref:`geo_subdfisc_parcelle`
- :ref:`geo_symblim_parcelle`
- :ref:`parcelle`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154) 
- geom_uf geometry(MultiPolygon,2154) - géométrie de l'unité foncière

.. image:: ../_static/utilisation/geo_parcelle_sig.jpg

.. _geo_subdfisc:

**********************************************
Subdivision fiscale de parcelle (geo_subdfisc)
**********************************************

.. note ::

  Partie d'une parcelle ayant une seule nature de culture ou de propriété 
  et constituant une unité au regard de la fiscalité directe locale.

  Incluse dans la parcelle qui la supporte. Les limites de la subdivision 
  fiscale peuvent être en partie confondues avec celles de la parcelle.  

.. image:: ../_static/utilisation/geo_subdfisc.jpg

Champ clé primaire :

- geo_subdfisc character varying(15) NOT NULL ([annee][tex][lot][incrément] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(1): Lettre d'ordre
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
  
Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_subdfisc_parcelle`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_subdfisc_sig.jpg

.. _geo_subdfisc_parcelle:

*********************
geo_subdfisc_parcelle
*********************

Lien Subdivision fiscale de parcelle <> parcelle

.. image:: ../_static/utilisation/geo_subdfisc_parcelle.jpg

Champ clé primaire :

- geo_subdfisc_parcelle serial NOT NULL
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
	  
Champs clés étrangères :

- :ref:`geo_subdfisc`
- :ref:`geo_parcelle`

.. _geo_voiep:

*******************************
Ensemble immobilier (geo_voiep)
*******************************

.. note ::

  Elément ponctuel permettant la gestion de l'ensemble immobilier auquel est associé son libellé.

  La gestion de l’ensemble immobilier est limitée au positionnement (obligatoire) de son nom . (ex. : Résidence Les Cigognes)

  Il n'a aucune emprise au sol. Son contour n'est pas décrit.

.. image:: ../_static/utilisation/geo_voiep.jpg

Champ clé primaire :

- geo_voiep character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(80): Nom de la voie
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
	  
Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_label`

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_voiep_sig.jpg

.. _geo_numvoie:

******************************
Numéro de voirie (geo_numvoie)
******************************

.. note ::

  Numéro correspondant à l'adresse de la parcelle(et non du bâtiment).

  Son positionnement est ponctuel, à l'extérieur de la parcelle. 
  Lorsqu'une parcelle comporte plusieurs numéros de voirie, tous 
  doivent être présents dans l'échange.

.. image:: ../_static/utilisation/geo_numvoie.jpg

Champ clé primaire :

- geo_numvoie character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(5): Numéro
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
	  
Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_numvoie_parcelle`

Champ(s) cartographique(s) :
- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_numvoie_sig.jpg

.. _geo_numvoie_parcelle:

********************
geo_numvoie_parcelle
********************

Lien Numéro de voirie <> parcelle

.. image:: ../_static/utilisation/geo_numvoie_parcelle.jpg

Champ clé primaire :

- geo_numvoie_parcelle serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
	  
Champs clés étrangères :

- :ref:`geo_numvoie`
- :ref:`geo_parcelle`

.. _geo_lieudit:

**********************
Lieu-dit (geo_lieudit)
**********************

.. note ::

  Ensemble de parcelles entières comportant une même dénomination géographique résultant de l'usage.
  
  La limite du "lieu-dit" ne coupe jamais des parcelles.
  
  Elément surfacique (contour fermé). A chaque objet "lieu-dit" est associé obligatoirement son libellé.

.. image:: ../_static/utilisation/geo_lieudit.jpg

Champ clé primaire :

- geo_lieudit character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(80): Libellé
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
  
Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_label`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_lieudit_sig.jpg

.. _geo_batiment:

***********************
Bâtiment (geo_batiment)
***********************

.. note ::

  Construction assise sur une ou plusieurs parcelles cadastrales.
  Un bâtiment est en relation avec chacune de ses parcelles d’assise.
  
  Dans le cas où le bâtiment se situe sur le domaine non cadastré, il est rattaché à la parcelle la plus proche.
  
  Les limites d'un bâtiment peuvent être en partie confondues avec celles des parcelles.
  Une division de parcelles n'entraîne pas une division de bâtiment.
  Lorsqu'on réunit deux parcelles sur lesquelles sont implantés deux bâtiments contigus, les bâtiments ne sont pas réunis.

.. image:: ../_static/utilisation/geo_batiment.jpg

Champ clé primaire :

- geo_batiment character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(80): Texte du bâtiment
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
	  
Champs clés étrangères :

- geo_dur_ character varying(2): Type de bâtiment

Table(s) dépendantes(s) :

- :ref:`geo_batiment_parcelle`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_batiment_sig.jpg

.. _geo_batiment_parcelle:

*********************
geo_batiment_parcelle
*********************

Lien Bâtiment <> Descriptif de parcelle (parcelle)

.. image:: ../_static/utilisation/geo_batiment_parcelle.jpg

Champ clé primaire :

- geo_batiment_parcelle serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
	  
Champs clés étrangères :

- :ref:`geo_batiment`
- :ref:`geo_parcelle`

.. _geo_zoncommuni:

**************************************
Zone de communication (geo_zoncommuni)
**************************************

.. note ::

  Voie du domaine non cadastré (ou passant sur des parcelles non figurées au plan), représentée par un élément linéaire correspondant à son axe.
  Le libellé de la voie concernée est porté par un ou plusieurs éléments linéaires de cet axe.
  
  Cet objet est constitué de l'axe médian de la voie.
  Il peut être utilisé comme support du libellé de la voie.
  Une zone de communication peut être confondue avec une limite de lieu-dit, de subdivision de section, 
  de section, de commune, de département ou d'Etat lorsque ceux-ci ont des parties communes.

.. image:: ../_static/utilisation/geo_zoncommuni.jpg

Champ clé primaire :

- geo_zoncommuni character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(255): Nom de la voie
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
	  
Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_label`

Champ(s) cartographique(s) :

- geom geometry(MultiLineString,2154)

.. image:: ../_static/utilisation/geo_zoncommuni_sig.jpg

.. _geo_tronfluv:

*************************************
Tronçon de cours d'eau (geo_tronfluv)
*************************************

.. note ::

  Elément surfacique (fermé) utilisé pour tous les cours d’eau, les étangs, les lacs, les rivages de mer... Un libellé y est associé.
  
  Les pièces d'eau sans libellé sont considérées comme des objets surfaciques divers.

.. image:: ../_static/utilisation/geo_tronfluv.jpg

Champ clé primaire :

- geo_tronfluv character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- tex character varying(255): Nom du cours d'eau
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données
	  
Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_label`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_tronfluv_sig.jpg

.. _geo_ptcanv:

*****************************
Point de canevas (geo_ptcanv)
*****************************

.. note ::

  Objet ponctuel servant d'appui aux opérations de lever des plans.

.. image:: ../_static/utilisation/geo_ptcanv.jpg

Champ clé primaire :

- geo_ptcanv character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- idu character varying(8): Identifiant PCI
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- geo_can_ character varying(2): Origine du point
- geo_ppln_ character varying(2): Précision planimétrique
- geo_palt_ character varying(2): Précision altimétrique
- geo_map_ character varying(2): Stabilité de matérialisation du support
- geo_sym_ character varying(2): Genre du point
  

Table(s) dépendantes(s) :

- néant

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_ptcanv_sig.jpg

.. _geo_borne:

****************************************
Borne de limite de propriété (geo_borne)
****************************************

.. note ::

  Borne située en limite de propriété et représentée par un symbole ponctuel.
  
  Cet objet est en relation avec l'ensemble des parcelles concernées.

.. image:: ../_static/utilisation/geo_borne.jpg

Champ clé primaire :

- geo_borne character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- néant

Table(s) dépendantes(s) :

- :ref:`geo_borne_parcelle`

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_borne_sig.jpg

.. _geo_borne_parcelle:

******************
geo_borne_parcelle
******************

Lien Borne de limite de propriété <> parcelle

.. image:: ../_static/utilisation/geo_borne_parcelle.jpg

Champ clé primaire :

- geo_borne_parcelle serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année

Champs clés étrangères :

- :ref:`geo_borne`
- :ref:`geo_parcelle`

.. _geo_croix:

*****************
Croix (geo_croix)
*****************

.. note ::

  Non documenté

.. image:: ../_static/utilisation/geo_croix.jpg

Champ clé primaire :

- geo_croix character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- néant  

Table(s) dépendantes(s) :

- :ref:`geo_croix_parcelle`

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_croix_sig.jpg

.. _geo_croix_parcelle:

******************
geo_croix_parcelle
******************

Lien Croix <> parcelle

.. image:: ../_static/utilisation/geo_croix_parcelle.jpg

Champ clé primaire :

- geo_croix_parcelle serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année

Champs clés étrangères :

- :ref:`geo_croix`
- :ref:`geo_parcelle`

.. _geo_symblim:

*******************************
Symbole de limite (geo_symblim)
*******************************

.. note ::

  Non documenté

.. image:: ../_static/utilisation/geo_symblim.jpg

Champ clé primaire :

- geo_symblim character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- ori numeric(12,9): Orientation  
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- geo_sym_ character varying(2): Genre

Table(s) dépendantes(s) :

- :ref:`geo_symblim_parcelle`

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_symblim_sig.jpg

.. _geo_symblim_parcelle:

****************************************
Symbole de limite (geo_symblim_parcelle)
****************************************

Lien Symbole de limite <> Parcelle.

Table non alimentée à ce jour.

Champ clé primaire :

- geo_symblim_parcelle serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année

Champs clés étrangères :

- :ref:`geo_symblim`
- :ref:`geo_parcelle`

.. _geo_tpoint:

**********************************
Objet ponctuel divers (geo_tpoint)
**********************************

.. note ::

  Détail topographique ponctuel représenté par un signe conventionnel de type ponctuel
  permettant de documenter le plan cadastral et d'en améliorer la lisibilité.

.. image:: ../_static/utilisation/geo_tpoint.jpg

Champ clé primaire :

- geo_tpoint character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- ori numeric(12,9): Orientation
- tex character varying(80): Texte du détail
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- geo_sym_ character varying(2): Genre
  
Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_tpoint_commune`

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_tpoint_sig.jpg

.. _geo_tpoint_commune:

******************
geo_tpoint_commune
******************

Lien Objet ponctuel divers <> commune

.. image:: ../_static/utilisation/geo_tpoint_commune.jpg

Champ clé primaire :

- geo_tpoint_commune serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année

Champs clés étrangères :

- :ref:`geo_tpoint`
- :ref:`geo_commune`

.. _geo_tline:

*********************************
Objet linéaire divers (geo_tline)
*********************************

.. note ::

  Détail topographique linéaire représenté par un signe conventionnel de type linéaire
  permettant de documenter le plan cadastral et d'en améliorer la lisibilité.

.. image:: ../_static/utilisation/geo_tline.jpg

Champ clé primaire :

- geo_tline character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- ori numeric(12,9): Orientation
- tex character varying(80): Texte du détail
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- geo_sym_ character varying(2): Genre
  
Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_tline_commune`

Champ(s) cartographique(s) :

- geom geometry(MultiLineString,2154)

.. image:: ../_static/utilisation/geo_tline_sig.jpg

.. _geo_tline_commune:

*****************
geo_tline_commune
*****************

Lien Objet linéaire divers <> Commune

.. image:: ../_static/utilisation/geo_tline_commune.jpg

Champ clé primaire :

- geo_tline_commune serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année

Champs clés étrangères :

- :ref:`geo_tline`
- :ref:`geo_commune`

.. _geo_tsurf:

***********************************
Objet surfacique divers (geo_tsurf)
***********************************

.. note ::

  Détail topographique surfacique représenté par un signe conventionnel de type surfacique
  permettant de documenter le plan cadastral et d'en améliorer la lisibilité.

.. image:: ../_static/utilisation/geo_tsurf.jpg

Champ clé primaire :

- geo_tsurf character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- object_rid character varying(80): Numéro d'objet
- ori numeric(12,9): Orientation
- tex character varying(80): Texte du détail
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- geo_sym_ character varying(2): Genre
  
Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_tsurf_commune`

Champ(s) cartographique(s) :

- geom geometry(MultiPolygon,2154)

.. image:: ../_static/utilisation/geo_tsurf_sig.jpg

.. _geo_tsurf_commune:

*****************
geo_tsurf_commune
*****************

Lien objet surfacique divers <> commune

.. image:: ../_static/utilisation/geo_tsurf_commune.jpg

Champ clé primaire :

- geo_tsurf_commune serial NOT NULL
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année

Champs clés étrangères :

- :ref:`geo_tsurf`
- :ref:`geo_commune`

 .. _geo_label:

*****************************
Ecriture-attribut (geo_label)
*****************************

.. note ::

  Objet de la norme EDIGéO utilisé pour le positionnement graphique
  d’un libellé ou d’un autre type d’écriture.

.. image:: ../_static/utilisation/geo_label.jpg

Champ clé primaire :

- geo_label character varying(14) NOT NULL ([annee][lot][incrément] caractère + remplacé par ¤ et espace par -)
 
Autres colonnes :

- annee character varying(4) NOT NULL: Année
- ogc_fid integer: Numéro d'enregistrement source
- object_rid character varying(80): Numéro d'objet
- fon character varying(80): Nom en clair de la police typographique
- hei numeric(24,15): Hauteur des caractères
- tyu character varying(80): Type de l'unité utilisée
- cef numeric(24,15): Facteur d'agrandissement
- csp numeric(24,15): Espacement intercaractères
- di1 numeric(24,15): Orientation composante X du vecteur hauteur
- di2 numeric(24,15): Orientation composante Y du vecteur hauteur
- di3 numeric(24,15): Orientation composante X du vecteur base
- di4 numeric(24,15): Orientation composante Y du vecteur base
- tpa character varying(80): Sens de l'écriture
- hta character varying(80): Alignement horizontal du texte
- vta character varying(80): Alignement vertical du texte
- atr character varying(80): Identificateur de l'attribut à écrire
- ogr_obj_ln character varying(80): lien n°objet
- ogr_obj__1 character varying(80): type objet
- ogr_atr_va character varying(80): Ogr valeur
- ogr_angle numeric(24,15): Ogr angle
- ogr_font_s numeric(24,15): Ogr taille fonte
- creat_date date: Date de création
- update_dat date: Date de dernière modification
- lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`geo_parcelle`
- :ref:`geo_section`
- :ref:`geo_voiep`
- :ref:`geo_tsurf`
- :ref:`geo_numvoie`
- :ref:`geo_lieudit`
- :ref:`geo_zoncommuni`
- :ref:`geo_tpoint`
- :ref:`geo_subdfisc`
- :ref:`geo_tline`
- :ref:`geo_tronfluv`

Table(s) dépendantes(s) :

- :ref:`geo_label`
- :ref:`geo_tsurf_commune`

Champ(s) cartographique(s) :

- geom geometry(Point,2154)

.. image:: ../_static/utilisation/geo_label_sig.jpg

-----------------
Les nomenclatures
-----------------

.. _geo_qupl:

- geo_qupl: Qualité du plan

.. _geo_copl:

- geo_copl: Mode de confection

.. _geo_inp:

- geo_inp: Mode d'incorporation au plan

.. _geo_indp:

- geo_indp: Figuration de la parcelle au plan

.. _geo_dur:

- geo_dur: Type de bâtiment

.. _geo_can:

- geo_can: Origine du point

.. _geo_ppln:

- geo_ppln: Précision planimétrique

.. _geo_palt:

- geo_palt: Précision altimétrique

.. _geo_map:

- geo_map: Stabilité de matérialisation du support

.. _geo_sym:

- geo_sym: Genre du point