.. _rub_pdl:

###########################
Propriétés divisées en lots
###########################

Important: les notes sont extraites de la documentation fournie avec les données.

.. note ::

  Le fichier des PDL et des lots est un fichier comportant la description des propriétés divisées en lots.

------------------
Modèle relationnel
------------------
.. image:: ../_static/modeles/pdl.jpg

------------------
Les tables
------------------

.. _pdl:

*********************************
Propriétés divisées en lots (pdl)
*********************************

.. note ::

  Est crée pour chaque PDL répertoriée dans MAJIC et est composé d'au moins un lot.
  Est unique et obligatoire.

.. image:: ../_static/utilisation/pdl.jpg

Champ clé primaire :

- pdl character varying(22) NOT NULL ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla][dnupdl] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :*

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - ccopre character varying(3): code du préfixe
  - ccosec character varying(2): lettres de section
  - dnupla character varying(4): numéro de plan
  - dnupdl character varying(3): no de pdl
  
* Descriptif de la PDL 

  - dnivim character varying(1): niveau imbrication
  - dnompdl character varying(30): ?
  - dmrpdl character varying(20): lot mère(plan+pdl+lot)
  - gprmut character varying(1): top 1ere mut effectuée
  - dnupro character varying(6): compte de la pdl
  - ccocif character varying(4): code cdif
  
* Autre

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`parcelle`: parcelle character varying(19) - parcelle à laquelle est rattachée la pdl
- ctpdl_ character varying(3): type de pdl - bnd, cl, cv, tf, clv, mp
- :ref:`comptecommunal`: comptecommunal character varying(12) - comptecommunal auquel est rattaché la pdl

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- lots: :ref:`lots`
- parcelle: :ref:`parcelle`
- parcellecomposante: :ref:`parcellecomposante`
- suf: :ref:`suf`

.. _parcellecomposante:

***********************************************
Parcelle composante de PDL (parcellecomposante)
***********************************************

.. note ::

  Créé pour chaque parcelle composante de la PDL, autre que la parcelle de
  référence, d'où la présence facultative de ce type d'article. 
  
  Exemple : un immeuble est construit sur 2 parcelles. La première sera 
  parcelle de référence et la seconde parcelle composante.

.. image:: ../_static/utilisation/parcellecomposante.jpg

Champ clé primaire :

- parcellecomposante character varying(31) NOT NULL ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla][dnupdl][ccoprea][ccoseca][dnuplaa] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

* Clé de la PDL

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - ccopre character varying(3): code du préfixe
  - ccosec character varying(2): lettres de section
  - dnupla character varying(4): numéro de plan
  - dnupdl character varying(3): no de pdl
  
* Identifiant parcelle d'assise

  - ccoprea character varying(3): code du préfixe
  - ccoseca character varying(2): lettres de section
  - dnuplaa character varying(4): numéro de plan
  - ccocif character varying(4): code cdif
  
* Autre

  - lot character(3): lot d'importation des données
  
Champs clés étrangères :

- :ref:`parcelle`: parcelle character varying(19) - parcelle à laquelle est rattachée la psl
- :ref:`pdl`: pdl character varying(22) - pdl à laquelle est rattachée la parcelle composante
- :ref:`parcelle`: parcellea character varying(19) - parcelle d'assise

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant

.. _lots:

**********
Lot (lots)
**********

.. note ::

  Créé pour chaque lot. 
  Tout article lot est précède d'un article pdl.

.. image:: ../_static/utilisation/lots.jpg

Champ clé primaire :

- lots character varying(29) NOT NULL ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla][dnupdl][dnulot] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département
- ccodir character varying(1): Code direction
- ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
- ccopre character varying(3): code du préfixe
- ccosec character varying(2): lettres de section
- dnupla character varying(4): numéro de plan
- dnupdl character varying(3): no de pdl
- dnulot character varying(7): Numéro de lot
- dcntlo integer: Superficie du lot
- dnumql integer: Numérateur
- ddenql integer: Dénominateur
- dfilot character varying(20): pdl-fille du lot
- datact date: date de l'acte
- dnuprol character varying(6): Compte du lot
- dreflf character varying(5): Référence livre foncier
- ccocif character varying(4): code cdif
- lot character(3)
 
Champs clés étrangères :

- :ref:`parcelle`: parcelle character varying(19) - parcelle à laquelle est rattachée le lot
- :ref:`pdl`: pdl character varying(22) - pdl à laquelle est rattachée le lot
- cconlo_ character varying(1): Code nature du lot
- :ref:`comptecommunal`: comptecommunal character varying(12) - comptecommunal auquel est rattaché le loy

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- lotslocaux: :ref:`lotslocaux`

------------------
Les nomenclatures
------------------

.. _ctpdl:

- ctpdl: Type de PDL - Il peut prendre les valeurs : BND, CL, CV, TF, CLV, MP.

.. _cconlo:

- cconlo:  Nature du lot