.. _rub_llot:

############
Lot et local
############

Important: les notes sont extraites de la documentation fournie avec les données.

.. note ::

  Le fichier Lot-Local est une table de correspondance entre les identifiants des locaux
  (numéro invariant) et les indicatifs des lots correspondants.

------------------
Modèle relationnel
------------------
.. image:: ../_static/modeles/lloc.jpg

------------------
Les tables
------------------

.. _lotslocaux:

**************************************************
Correspondance des locaux et des lots (lotslocaux)
**************************************************

.. image:: ../_static/utilisation/lotslocaux.jpg

Champ clé primaire :
 - lotslocaux character varying(39) NOT NULL ([lot][invloc] caractère + remplacé par ¤ et espace par -)
  
Autres colonnes :

* Identifiant d'un lot

  - annee character varying(4)
  - ccodepl character varying(2): Lot - Code département
  - ccodirl character varying(1): Lot - Code direction
  - ccocoml character varying(3): Lot - Code INSEE de la commune
  - ccoprel character varying(3): Lot - Code préfixe
  - ccosecl character varying(2): Lot - Code section
  - dnuplal character varying(4): Lot - Numéro du plan
  - dnupdl character varying(3): Lot - Numéro de PDL
  - dnulot character varying(7): Lot - Numéro de lot
  
* Identifiant d'un local

  - ccodebpb character varying(2): Local - Code département
  - ccodird character varying(1): Local - Code direction
  - ccocomb character varying(3): Local - Code commune
  - ccopreb character varying(3): Local - Code préfixe
  - invloc character varying(10): Local - Numéro invariant du local
  - dnumql character varying(7): Local - Numérateur du lot
  - ddenql character varying(7): Local - Dénominateur du lot
  
* Autre

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`lots`: lots character varying(29)
- :ref:`local00`: local00 character varying(14)
- :ref:`local10`: local10 character varying(14)

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant

------------------
Les nomenclatures
------------------

néant
