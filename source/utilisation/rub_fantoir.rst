.. _rub_fantoir:

#######
Fantoir
#######

Important: les notes sont extraites de la documentation fournie avec les données.

.. note ::

  Le fichier des voies et lieux-dits ou fichier FANTOIR recense par commune :
   - les voies ;
   - les lieux-dits ;
   - les ensembles immobiliers ;
   - les pseudo-voies.

  Il est constitué de l’ensemble des références topographiques qu’elles soient annulées ou actives.

------------------
Modèle relationnel
------------------
.. image:: ../_static/modeles/fantoir.jpg

------------------
Les tables
------------------

.. _commune:

*****************
Commune (commune)
*****************
Liste des communes

.. image:: ../_static/utilisation/commune.jpg

Champ clé primaire :

- commune character varying(10) NOT NULL : code identifiant la commune ([annee][ccodep][ccodir][ccocom])

Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département - Code département INSEE
- ccodir character varying(1): Code direction - Code direction dge
- ccocom character varying(3): Code commune - code commune définie par Majic2
- clerivili character varying(1):  Clé RIVOLI - zone alphabétique fournie par MAJIC2
- libcom character varying(30): Libellé - désignation de la commune
- ruract character varying(1): Caractère RUR (3-commune pseudo-recensée, blanc si rien)
- *carvoi character varying(1): Caractère de voie (indisponible)*
- indpop character varying(1): indicateur de population - Précise la dernière situation connue de la commune au regard de la limite de 3000 habitants (= blanc si < 3000 h sinon = "*").
- poprel integer: population réelle - dénombre la population recencée lors du dernier recensement
- poppart integer: population à part - dénombre la population comptée à part dans la commune
- popfict integer: population fictive - population fictive de la commune
- annul character varying(1): Annulation Cet article indique que plus aucune entité topo n’est représentée par ce code. - O - voie annulée sans transfert, Q - voie annulée avec transfert, Q - commune annulée avec transfert.
- dteannul character varying(7): date d'annulation
- dtecreart character varying(7): Date de création de l'article - Date à laquelle l'article a été créé par création MAJIC2.
- *codvoi character varying(5): Code identifiant la voie dans MAJIC2. - Permet de faire le lien entre le code voie RIVOLI et le code voie MAJIC2. (indisponible)*
- *typvoi character varying(1): Type de voie - Indicateur de la classe de la voie. - 1 - voie, 2 - ensemble immobilier, 3 - lieu-dit, 4 - - pseudo-voie, 5 - voie provisoire. (indisponible)*
- *indldnbat character varying(1): Indicateur lieu-dit non bâti - Zone servie uniquement pour les lieux-dits.Permet d’indiquer si le lieu-dit comporte ou non un bâtiment dans MAJIC.1 pour lieu-dit non bâti, 0 sinon. (indisponible)*
- *motclas character varying(8): Mot classant - Dernier mot entièrement alphabétique du libellé de voie - Permet de restituer l'ordre alphabétique. (indisponible)*
- lot character(3): lot d'importation des données

Champs clés étrangères :

- typcom_ character varying(1): Type de commune actuel (R ou N) - N - commune rurale, R - commune rencencée

Lien cartographique :

- geo_commune: :ref:`geo_commune`

Table(s) dépendantes(s) :

- voie: :ref:`voie`

.. _voie:

*************
Voies (voie)
*************

Liste des voies, lieux-dits, ensembles immobilierset pseudo-voies.

.. image:: ../_static/utilisation/voie.jpg

Champ clé primaire :

- voie character varying(15) NOT NULL: identifiant ([annee][ccodep][ccodir][ccocom][ccoriv])

Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département - Code département INSEE
- ccodir character varying(1): Code direction - Code direction dge
- ccocom character varying(3): Code commune - code commune définie par Majic2
- ccoriv character varying(4): Code voie Rivoli - identifiant de voie dans la commune
- clerivili character varying(1): Clé RIVOLI - zone alphabétique fournie par MAJIC2
- natvoi character varying(4): nature de voie
- libvoi character varying(26): libellé de voie
- ruract character varying(1): RUR actuel - indique si la commune est pseudo-recensée ou non (3-commune pseudo-recensée, blanc si rien)
- indpop character varying(1): indicateur de population - Précise la dernière situation connue de la commune au regard de la limite de 3000 habitants (= blanc si < 3000 h sinon = "*").
- *poprel character varying(7): population réelle - dénombre la population recencée lors du dernier recensement (indisponible)*
- poppart integer: population à part - dénombre la population comptée à part dans la commune
- popfict integer: population fictive - population fictive de la commune
- dteannul character varying(7): date d'annulation
- dtecreart character varying(7): Date de création de l'article - Date à laquelle l'article a été créé par création MAJIC2.
- codvoi character varying(5): Code identifiant la voie dans MAJIC2. - Permet de faire le lien entre le code voie RIVOLI et le code voie MAJIC2.
- motclas character varying(8): Mot classant - Dernier mot entièrement alphabétique du libellé de voie - Permet de restituer l'ordre alphabétique.
- lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`commune` : commune character varying(10)
- natvoiriv_ character varying(1): Nature de voie rivoli
- typcom_ character varying(1): Type de commune actuel (R ou N) - N - commune rurale, R - commune rencencée
- carvoi_ character varying(1): caractère de voie - zone indiquant si la voie est privée (1) ou publique (0)
- annul_ character varying(1): Annulation Cet article indique que plus aucune entité topo n’est représentée par ce code. - O - voie annulée sans transfert, Q - voie annulée avec transfert, Q - commune annulée avec transfert.
- typvoi_ character varying(1): Type de voie - Indicateur de la classe de la voie. - 1 - voie, 2 - ensemble immobilier, 3 - lieu-dit, 4 - - pseudo-voie, 5 - voie provisoire.
- indldnbat_ character varying(1): Indicateur lieu-dit non bâti - Zone servie uniquement pour les lieux-dits.Permet d’indiquer si le lieu-dit comporte ou non un bâtiment dans MAJIC.1 pour lieu-dit non bâti, 0 sinon.

Table(s) détail :

- local00: :ref:`local00`
- local10: :ref:`local10`
- parcelle: :ref:`parcelle`

-----------------
Les nomenclatures
-----------------

.. _typcom: 

- typcom : Type de commune actuel (R ou N); N: commune rurale, R: commune rencencée

.. _natvoiriv: 

- natvoiriv: Nature de voie rivoli

.. _carvoi: 

- carvoi: caractère de voie - zone indiquant si la voie est privée (1) ou publique (0)
 
.. _annul:

- annul: Annulation Cet article indique que plus aucune entité topo n’est représentée par ce code O: voie annulée sans transfert, Q : commune/voie annulée avec transfert

.. _typvoi: 

- typvoi: Type de voie - Indicateur de la classe de la voie. 1: voie, 2: ensemble immobilier, 3: lieu-dit, 4: pseudo-voie, 5: voie provisoire

.. _indldnbat: 

- indldnbat: Indicateur lieu-dit non bâti - Zone servie uniquement pour les lieux-dits. Permet d’indiquer si le lieu-dit comporte ou non un bâtiment dans MAJIC. 1 pour lieu-dit non bâti, 0 sinon

