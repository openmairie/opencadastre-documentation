.. _rub_nbat:

#####################
Propriétés non baties
#####################

Important: les notes sont extraites de la documentation fournie avec les données.

.. note ::
  
  Le fichier des propriétés non bâties ou fichier parcellaire recense l'ensemble des parcelles et
  des subdivisions fiscales cadastrées en France.
  
  La parcelle permet de définir le propriétaire, la subdivision fiscale (ou suf) est l’unité
  élémentaire d’évaluation en non bâti. Par exemple la parcelle X appartient au propriétaire Y
  et est composée de deux sufs, l'une correspondant à une vigne, l'autre à une terre.

  De niveau inférieur à la parcelle, la suf correspond soit à une subdivision fiscale, soit à une
  parcelle non subdivisée : une parcelle peut être cadastralement non subdivisée et, dans ce
  cas, elle est composée d'une seule suf, ou être découpée en N Subdivisions fiscales et dans
  ce cas, elle est composée de N sufs.
  
  Ce fichier permet de connaître, pour une parcelle donnée, les natures de culture ou de
  propriété, les contenances et les revenus cadastraux des subdivisions fiscales qui la
  composent, ainsi que l'attribution à un compte communal de propriétaire.

------------------
Modèle relationnel
------------------
.. image:: ../_static/modeles/nbat.jpg

------------------
Les tables
------------------

.. _parcelle:

*********************************
Descriptif de parcelle (parcelle)
*********************************

.. note ::

  Contient les données générales de la parcelle, en particulier
  l'attribution et l'adresse. Sa présence est systématique

.. image:: ../_static/utilisation/parcelle.jpg

Champ clé primaire :

- parcelle character varying(19) NOT NULL: code identifiant la parcelle ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - ccopre character varying(3): Préfixe de section ou quartier servi pour les communes associées
  - ccosec character varying(2): Section cadastrale
  - dnupla character varying(4): Numéro de plan
  - dcntpa integer: Contenance de la parcelle - en centiares
  - dsrpar character varying(1): Lettre de série-role
  - dnupro character varying(6): Compte communal du propriétaire de la parcelle
  - jdatat date: Date de l acte - jjmmaaaa
  - dreflf character varying(5): Référence au Livre Foncier en Alsace-Moselle

* Identifiant de la PDL

  - cprsecr character varying(3): Préfixe de la parcelle de référence
  - ccosecr character varying(2): Section de la parcelle de référence
  - dnuplar character varying(4): N° de plan de la parcelle de référence
  - dnupdl character varying(3): Numéro d’ordre de la pdl - en général 001
  - gurbpa character varying(1): Caractère Urbain de la parcelle - U, * ou blanc
  - dparpi character varying(4): Numéro de parcelle primitive
  - ccoarp character varying(1): Indicateur d’arpentage - A ou blanc
  - gparnf character varying(1): Indicateur de parcelle non figurée au plan - 1 = figurée, 0 = non figurée
  - gparbat character varying(1): Indicateur de parcelle référençant un bâtiment - 1 = oui, sinon 0
  - *parrev character varying(12): Info de la révision  (indisponible)*
  - *gpardp character varying(1): parcelle n'appartenant pas au domaine public  (indisponible)*
  - *fviti character varying(1): parcelle au casier viticole  Adresse de la parcelle  (indisponible)*
  
* Adresse de la parcelle

  - dnvoiri character varying(4): Numéro de voirie
  - dindic character varying(1): Indice de répétition
  - ccovoi character varying(5): Code Majic2 de la voie
  - ccoriv character varying(4): Code Rivoli de la voie
  - ccocif character varying(4): Code du cdif (code topad)
  - *gpafpd character varying(1): Domanialité, représentation au plan  (indisponible)*
  
* Libellé de la voie de la voie de la parcelle 

  - cconvo character varying(4): Code nature de la voie
  - dvoilib character varying(26): Libellé de la voie
  
* Filiation de la parcelle 

  - ccocomm character varying(3): Code INSEE de la commune de la parcelle mère
  - ccoprem character varying(3): Code du préfixe de section de la parcelle mère
  - ccosecm character varying(2): Code section de la parcelle mère
  - dnuplam character varying(4): Numéro de plan de la parcelle mère
  - parcellefiliation character varying(19): Parcelle en filiation
  - type_filiation character varying(1): Type de filiation (D, R, T ou blanc)
  
* autres  

  - ajoutcoherence character varying(1): indicateur de cohérence - N: présent dans le fichier majic3 NBAT, O: absent du fichier majic3 NBAT, ajouté pour assurer l'intégrité reférentielle avec les tables dépendantes
  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`comptecommunal`: comptecommunal character varying(12) - référence au compte communal auquel la parcelle est rattachée
- gpdl_ character varying(1): Indicateur d’appartenance à pdl
- :ref:`pdl`: pdl character varying(22) - Propriétés divisées en lots à laquelle se réfère la parcelle
- :ref:`voie`: voie character varying(15) - Voie à laquelle est adossé la parcelle en terme d'adressage
- :ref:`parcelle`: parcelle_uf character varying(19) - lien sur la parcelle de référence de l'unité foncière à laquelle appartient la parcelle

Lien(s) cartographique(s) :

- :ref:`geo_parcelle`: geo_parcelle character varying(16) - lien geo_parcelle (représentation cartographique de la parcelle si elle figure au plan cadastral numérisé)

Table(s) dépendantes(s) :

- local00: :ref:`local00`
- local10: :ref:`local10`
- lots: :ref:`lots`
- parcelle: :ref:`parcelle`
- parcellecomposante: :ref:`parcellecomposante`
- pdl: :ref:`pdl`
- suf: :ref:`suf`
 
.. _suf:

*************************
Subdivision fiscale (suf)
*************************

.. note ::

  Contient les données descriptives de la suf (contenance, classement et évaluation), 
  la liaison éventuelle avec un lot de copropriété et l'attribution - qui peut être
  différente de celle de la parcelle en cas de liaison avec un lot.

.. image:: ../_static/utilisation/suf.jpg

Champ clé primaire :

- suf character varying(21) NOT NULL: code identifiant la subdivision fiscale ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla][ccosub] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département
  - ccodir character varying(1): Code direction
  - ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
  - ccopre character varying(3): Préfixe de section ou quartier servi pour les communes associées
  - ccosec character varying(2): Section cadastrale
  - dnupla character varying(4): Numéro de plan
  - ccosub character varying(2): Lettres indicatives de suf
  - dcntsf integer: Contenance de la suf - en centiares
  - dnupro character varying(6): Compte communal du propriétaire de la suf
  - drcsub numeric(10,2): Revenu cadastral en valeur actualise référence 1980 en €
  - drcsuba numeric(10,2): Revenu cadastral revalorisé en valeur du 01-01 de l’année en €
  - ccostn character varying(1): Série-tarif - A à Z, sauf I,O,Q
  - dclssf character varying(2): Classe dans le groupe et la série-tarif
  - drgpos character varying(1): Top terrain constructible Liaison avec un lot de pdl - « 0 » ou « 9 »
  
* Liaison avec un lot de PDL

  - ccoprel character varying(3): Préfixe de la parcelle identifiant le lot
  - ccosecl character varying(2): Section de la parcelle identifiant le lot
  - dnuplal character varying(4): N° de plan de la parcelle de référence
  - dnupdl character varying(3): Numéro d ordre de la pdl - en général, 001
  - dnulot character varying(7): Numéro du lot - Le lot de BND se présente sous la forme 00Axxxx
  - rclsi character varying(46): Données classement révisé (indisponible)*
  - gnidom character varying(1): Indicateur de suf non imposable - * ou blanc
  - topja character varying(1): Indicateur jeune agriculteur - J ou blanc
  - datja date: Date d’installation jeune agriculteur - peut être servie si topja = J
  - postel character varying(1): Indicateur de bien appartenant à la Poste - X ou blanc
  
* autres  

  - lot character(3): lot d'importation des données - autres  

Champs clés étrangères :

- :ref:`parcelle`: parcelle character varying(19) - parcelle à laquelle se réfère la suf
- :ref:`comptecommunal`: comptecommunal character varying(12) - référence au compte communal auquel la suf est rattachée
- gnexps_ character varying(2): Code exonération permanente - ep cd cr dr ni rt
- cgrnum_ character varying(2):  Groupe de nature de culture - 01 à 13
- dsgrpf_ character varying(2): Sous-groupe alphabétique
- cnatsp_ character varying(5): code nature de culture spéciale
- :ref:`pdl`: pdl character varying(22) - Propriétés divisées en lots à laquelle se réfère la suf

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- suftaxation: :ref:`suftaxation`
- sufexoneration: :ref:`sufexoneration`

.. _suftaxation:

*****************************
Taxation de suf (suftaxation)
*****************************

.. note ::

  Contient par collectivité concerne la base d'imposition revalorisée. Pour la
  seule occurrence « commune », un montant revalorisé correspondant à la « majoration
  POS » peut être servi. Les occurrences se présentent dans l'ordre suivant :
  - Part communale ;
  - Part départementale ;
  - Part régionale ;
  - Part du groupement de communes.

.. image:: ../_static/utilisation/suftaxation.jpg

Champ clé primaire :

- suftaxation character varying(21) NOT NULL: code identifiant la subdivision fiscale ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla][ccosub] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département
- ccodir character varying(1): Code direction
- ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
- ccopre character varying(3): Préfixe de section ou quartier servi pour les communes associées
- ccosec character varying(2): Section cadastrale
- dnupla character varying(4): Numéro de plan
- ccosub character varying(2): Lettres indicatives de suf
- c1majposa numeric(10,2): c1 - Montant de la majoration terrain constructible. Servi pour la part communale. Toujours à zéro pour autres collectivités en €
- c1bisufad numeric(10,2): c1 - Base d’imposition de la suf en valeur de l’année exprimé en Euros
- c2majposa numeric(10,2): c2 - Montant de la majoration terrain constructible. Servi pour la part communale. Toujours à zéro pour autres collectivités en €
- c2bisufad numeric(10,2): c2 - Base d’imposition de la suf en valeur de l’année exprimé en Euros
- c3majposa numeric(10,2): c3 - Montant de la majoration terrain constructible. Servi pour la part communale. Toujours à zéro pour autres collectivités en €
- c3bisufad numeric(10,2): c3 - Base d’imposition de la suf en valeur de l’année exprimé en Euros
- c4majposa numeric(10,2): c4 - Montant de la majoration terrain constructible. Servi pour la part communale. Toujours à zéro pour autres collectivités en €
- c4bisufad numeric(10,2): c4 - Base d’imposition de la suf en valeur de l’année exprimé en Euros
- cntmajtc integer: Nouvelle contenance suf pour calcul majoration TC
- lot character(3): lot d'importation des données - autres  

Champs clés étrangères :

- :ref:`suf`: suf character varying(21) - suf à laquelle est rattachée la taxation

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant

.. _sufexoneration:

***********************************
Exonération de suf (sufexoneration)
***********************************

.. note ::

  Vontient l’exonération temporaire de la suf à laquelle il est rattaché. Une suf
  peut théoriquement bénéficier de 0 à 4 exonérations différentes : le numéro d'ordre de
  l'exonération 30 varie donc de 01 a 04.

.. image:: ../_static/utilisation/sufexoneration.jpg

Champ clé primaire :

- sufexoneration character varying(21) NOT NULL: code identifiant la subdivision fiscale ([annee][ccodep][ccodir][ccocom][ccopre][ccosec][dnupla][ccosub][rnuexn] caractère + remplacé par ¤ et espace par -)

Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département
- ccodir character varying(1): Code direction
- ccocom character varying(3): Code commune INSEE ou DGI d’arrondissement
- ccopre character varying(3): Préfixe de section ou quartier servi pour les communes associées
- ccosec character varying(2): Section cadastrale
- dnupla character varying(4): Numéro de plan
- ccosub character varying(2): Lettres indicatives de suf
- rnuexn character varying(2): Numéro d ordre d’exonération temporaire - 01 à 04
- vecexn numeric(10,2): Montant de VL sur lequel porte l’exonération - en Euros
- pexn integer: Pourcentage d’exonération - 100
- jandeb character varying(4): Année de début d’exonération - à blanc
- jfinex character varying(4): Année de retour à imposition - à blanc
- fcexn character varying(10): Fraction de vecsuf exonérée (indisponible)*
- fcexna character varying(10): fcexn en année N (indisponible)*
- rcexna character varying(10): revenu (4/5 fcexna) correspondant (indisponible)*
- rcexnba numeric(10,2): Revenu cadastral exonéré, en valeur de l’année - Exprimé en Euros
- mpexnba character varying(10): Fraction majo TC exonérée, en valeur de l’année (indisponible)*
- lot character(3): lot d'importation des données - autres  

Champs clés étrangères :

- :ref:`suf`: suf character varying(21) - suf à laquelle est rattachée l'exonération
- ccolloc_ character varying(2): Collectivité accordant l’exonération - TC, C, R d OU GC
- gnexts_ character varying(2): Code d’exonération temporaire - TA TR NO PB PP PR PF ER TU OL HP HR ou NA
 
Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant

------------------
Les nomenclatures
------------------

.. _gpdl:

- gpdl : Indicateur d’appartenance à pdl

.. _gnexps:

- gnexps : Code d’exonération permanente

.. _cgrnum:

- cgrnum : Groupe nature de culture

.. _dsgrpf:

- dsgrpf : Sous groupe de suf

.. _cnatsp:

- cnatsp : code de la nature de culture spéciale

.. _ccolloc:

- ccolloc : Collectivité accordant l’exonération

.. _gnexts:

- gnexts : Codes « Exonération temporaire »
